
package Modelo;

import java.util.Objects;
/**
 *  Clase representa una persona con su fecha de nacimiento
 * @author ingrid
 */
public class Persona implements Comparable{
    
    private String nombresCompletos;
    //Su fecha de nacimiento
    private short dia, mes,agno, hora, min, seg;

    /**
     * crea una persona vacia
     */
    public Persona() {
    }

    /**
     * crea una persona
     * @param nombresCompletos
     * @param dia
     * @param mes
     * @param agno
     * @param hora
     * @param min
     * @param seg 
     */
    public Persona(String nombresCompletos, short dia, short mes, short agno, short hora, short min, short seg) {
        this.nombresCompletos = nombresCompletos;
        this.dia = dia;
        this.mes = mes;
        this.agno = agno;
        this.hora = hora;
        this.min = min;
        this.seg = seg;
    }
    
    /**
     *  Retorna el valor del los nombres completos
     * @return 
     */
    public String getNombresCompletos() {
        return nombresCompletos;
    }

    /**
     * Retorna el valor del Dia
     * @return 
     */
    public short getDia() {
        return dia;
    }
    
     /**
     * Retorna el valor del Mes
     * @return 
     */
    public short getMes() {
        return mes;
    }

     /**
     * Retorna el valor del Año
     * @return 
     */
    public short getAgno() {
        return agno;
    }

     /**
     * Retorna el valor de la Hora
     * @return 
     */
    public short getHora() {
        return hora;
    }
    
     /**
     * Retorna el valor del los Minutos
     * @return 
     */
    public short getMin() {
        return min;
    }

     /**
     * Retorna el valor del los Segundos
     * @return 
     */
    public short getSeg() {
        return seg;
    }

    /**
     * Actualiza el valor de los nombres completos
     * @param nombresCompletos 
     */
    public void setNombresCompletos(String nombresCompletos) {
        this.nombresCompletos = nombresCompletos;
    }

    /**
     * Actualiza el valor del Dia
     * @param dia 
     */
    public void setDia(short dia) {
        this.dia = dia;
    }

    /**
     * Actualiza el valor del mes
     * @param mes 
     */
    public void setMes(short mes) {
        this.mes = mes;
    }

    /**
     * Actualiza el valor del año
     * @param agno 
     */
    public void setAgno(short agno) {
        this.agno = agno;
    }

    /**
     * Actualiza el valor del la hora
     * @param hora 
     */
    public void setHora(short hora) {
        this.hora = hora;
    }

    /**
     * Actualiza el valor de los minutos
     * @param min 
     */
    public void setMin(short min) {
        this.min = min;
    }

    /**
     * Actualiza el valor de los segundos
     * @param seg 
     */
    public void setSeg(short seg) {
        this.seg = seg;
    }

    @Override
    public String toString() {
        return nombresCompletos+" Nacid@ el dia "+dia+" del mes de " + mes + " del año " + agno +" a las "+hora+" horas con "+min+" minutos y "+seg+" segundos";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.nombresCompletos);
        hash = 59 * hash + this.dia;
        hash = 59 * hash + this.mes;
        hash = 59 * hash + this.agno;
        hash = 59 * hash + this.hora;
        hash = 59 * hash + this.min;
        hash = 59 * hash + this.seg;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (this.dia != other.dia) {
            return false;
        }
        if (this.mes != other.mes) {
            return false;
        }
        if (this.agno != other.agno) {
            return false;
        }
        if (this.hora != other.hora) {
            return false;
        }
        if (this.min != other.min) {
            return false;
        }
        if (this.seg != other.seg) {
            return false;
        }
        if (!Objects.equals(this.nombresCompletos, other.nombresCompletos)) {
            return false;
        }
        return true;
    }

    /**
     * 
     * @param obj el objeto a comparar
     * @return un entero con la comparación(factor)
     */
    @Override
    public int compareTo(Object obj) {

        if (this == obj) {
            return 0;
        }
        if (obj == null) { 
            throw new RuntimeException("No se puede comparar por que el segundo objeto es null");
        }
        if (getClass() != obj.getClass()) {
            throw new RuntimeException("Objetos no son de la misma Clase");
        }
        final Persona other = (Persona) obj;
         
        long msga, msgb;
        
        /**
         * se pasan los valores a segundos y se suman para obtener un unico valor
         */
        msga = (this.agno * 31536000) + (this.mes * 2628000) + (this.dia * 86400) + (this.hora * 3600) + (this.min * 60) + this.seg ;
        msgb = (other.agno * 31536000) + (other.mes * 2628000) + (other.dia * 86400) + (other.hora * 3600) + (other.min * 60) + other.seg;

        if (msga < msgb){
            return 1;
        }else if (msga == msgb){
            return 2;
        }
        else {
            return 0;
        }
    }
}