
package Vista;

import Modelo.Persona;
import java.util.ArrayList;

/**
 *
 * @author BunnyMoon
 */
public class TestPersona {
    public static void main(String[] args) {
        
        ArrayList<Persona> personas = new ArrayList(); 
        Persona p1 = new Persona("Ingrid Florez ", (short)26, (short)02, (short)2002, (short)11, (short)30, (short)5);
        Persona p2 = new Persona("Angelis Duarte", (short)16, (short)06, (short)2001, (short)5, (short)45, (short)12);
        Persona p3 = new Persona("Jose Tarazona", (short)29, (short)02, (short)2019, (short)22, (short)25, (short)6);
        Persona p4 = new Persona("Darcy Florez", (short)29, (short)02, (short)2019, (short)22, (short)25, (short)6);
        
        personas.add(p1);
        personas.add(p2);
        personas.add(p3);
        personas.add(p4);
        
       for(int i = 0; i < personas.size(); i++){
           for(int j=0; j < personas.size(); j++){
               if (personas.get(i).equals(personas.get(j))){
                   System.out.println(personas.get(i).getNombresCompletos() + " es la misma persona que " + personas.get(j).getNombresCompletos());
                }else if(personas.get(i).compareTo(personas.get(j)) > 1 ){
                   System.out.println(personas.get(i).getNombresCompletos() + " tiene la misma edad que " + personas.get(j).getNombresCompletos());
                }else if (personas.get(i).compareTo(personas.get(j)) == 1){
                    System.out.println(personas.get(i).getNombresCompletos()+ " Es mayor que " + personas.get(j).getNombresCompletos());
                }else {
                    System.out.println(personas.get(i).getNombresCompletos()+ "Es menor que " + personas.get(j).getNombresCompletos());
                }
            }    
        }
    }
}
